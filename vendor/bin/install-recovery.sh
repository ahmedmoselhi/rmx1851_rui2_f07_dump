#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:67108864:8aa3a4bfee8ed1a03ab9321d1254dce941fc579b; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:6612cc3982ac6624bc776613dffc52c2a7429d43 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:67108864:8aa3a4bfee8ed1a03ab9321d1254dce941fc579b && \
      log -t recovery "Installing new oppo recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oppo recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
